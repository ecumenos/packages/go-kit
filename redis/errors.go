package redis

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Redis Codes
var (
	// IDCodeRedisPingErr ???
	IDCodeRedisPingErr = code.MustNewIDCode(code.RedisPrefix, 0)
	// IDCodeRedisGetErr ???
	IDCodeRedisGetErr = code.MustNewIDCode(code.RedisPrefix, 1)
	// IDCodeRedisSetErr ???
	IDCodeRedisSetErr = code.MustNewIDCode(code.RedisPrefix, 2)
	// IDCodeRedisDeleteErr ???
	IDCodeRedisDeleteErr = code.MustNewIDCode(code.RedisPrefix, 3)
	// IDCodeRedisCountErr ???
	IDCodeRedisCountErr = code.MustNewIDCode(code.RedisPrefix, 4)
	// IDCodeRedisExecCommandErr ???
	IDCodeRedisExecCommandErr = code.MustNewIDCode(code.RedisPrefix, 5)
	// IDCodeRedisConsumeErr ???
	IDCodeRedisConsumeErr = code.MustNewIDCode(code.RedisPrefix, 6)
	// IDCodeRedisPublishErr ???
	IDCodeRedisPublishErr = code.MustNewIDCode(code.RedisPrefix, 7)
)
