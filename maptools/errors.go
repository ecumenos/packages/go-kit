package maptools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Map Utilities Codes
var (
	// IDCodePickByProbErr ???
	IDCodePickByProbErr = code.MustNewIDCode(code.MapUtilsPrefix, 0)
	// IDCodePickOneImpossibleForNoValues ???
	IDCodePickOneImpossibleForNoValues = code.MustNewIDCode(code.MapUtilsPrefix, 1)
	// IDCodePickOneImpossibleForAllZeros ???
	IDCodePickOneImpossibleForAllZeros = code.MustNewIDCode(code.MapUtilsPrefix, 2)
	// IDCodeUnmarshalToMapErr ???
	IDCodeUnmarshalToMapErr = code.MustNewIDCode(code.MapUtilsPrefix, 3)
)
