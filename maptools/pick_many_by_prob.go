package maptools

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// PickManyByProb ???
func PickManyByProb[T string](ctx context.Context, values map[T]float64, l int) ([]T, error) {
	result := make([]T, 0, l)
	for {
		val, err := PickOneByProb(ctx, values)
		if err != nil {
			return nil, wrappederror.New(err, "Can not pick several random from input", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodePickByProbErr)))
		}
		if includes(result, val) {
			continue
		}
		result = append(result, val)
		if len(result) == l {
			break
		}
	}

	return result, nil
}

// includes ???
func includes[T string](values []T, v T) bool {
	for _, val := range values {
		if val == v {
			return true
		}
	}

	return false
}
