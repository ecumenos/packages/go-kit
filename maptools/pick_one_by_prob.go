package maptools

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"gitlab.com/ecumenos/packages/go-kit/randomtools"
)

// PickOneByProb ???
func PickOneByProb[T string](ctx context.Context, values map[T]float64) (T, error) {
	var zero T
	if len(values) == 0 {
		return zero, wrappederror.New(nil, "Can not pick value from empty map", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodePickOneImpossibleForNoValues)))
	}

	var (
		valuesWithZeroCount int
		valuesWith1Count    int
	)
	for _, prob := range values {
		if prob == 0 {
			valuesWithZeroCount++
		}
		if prob == 1 {
			valuesWith1Count++
		}
	}
	if valuesWithZeroCount == len(values) {
		return zero, wrappederror.New(nil, "Can not pick value from map will all zeros", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodePickOneImpossibleForAllZeros)))
	}
	if valuesWith1Count > 1 {
		values = PrepareMapToPickRandomValue(values)
	}

	preparedValues := Clone(values)
	for value, prob := range preparedValues {
		preparedValues[value] = randomtools.PrepareProbability(prob - 0.01)
	}

	tempValues := make(map[T]float64)
	for {
		var iterValues map[T]float64
		if len(tempValues) == 0 {
			iterValues = Clone(preparedValues)
		} else {
			iterValues = Clone(tempValues)
		}
		tempValues = make(map[T]float64)

		for value, prob := range iterValues {
			if r, _ := randomtools.GenBool(ctx, prob); r {
				tempValues[value] = prob
			}
		}

		if len(tempValues) == 1 {
			for value := range tempValues {
				return value, nil
			}
		}
	}
}

// PrepareMapToPickRandomValue ???
func PrepareMapToPickRandomValue[T string](values map[T]float64) map[T]float64 {
	var totalScore float64
	for _, score := range values {
		totalScore += score
	}

	out := make(map[T]float64, len(values))
	for key, value := range values {
		out[key] = value / totalScore
	}

	return out
}
