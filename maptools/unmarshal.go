package maptools

import (
	"context"
	"encoding/json"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Unmarshal []byte into map[string]interface{}
func Unmarshal(ctx context.Context, in []byte) (map[string]interface{}, error) {
	out := make(map[string]interface{})
	if err := json.Unmarshal(in, &out); err != nil {
		return nil, wrappederror.New(err, "Can not unmarshal bytes into map", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeUnmarshalToMapErr)))
	}

	return out, nil
}
