package postgresql

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// PostgreSQL Codes
var (
	// IDCodePostgreSQLPingError ???
	IDCodePostgreSQLPingErr = code.MustNewIDCode(code.PostgreSQLPrefix, 0)
	// IDCodePostgreSQLAcquireConnError ???
	IDCodePostgreSQLAcquireConnErr = code.MustNewIDCode(code.PostgreSQLPrefix, 1)
	// IDCodePostgreSQLClosePoolError ???
	IDCodePostgreSQLClosePoolErr = code.MustNewIDCode(code.PostgreSQLPrefix, 2)
	// IDCodePostgreSQLInsertError ???
	IDCodePostgreSQLInsertErr = code.MustNewIDCode(code.PostgreSQLPrefix, 3)
	// IDCodePostgreSQLFindError ???
	IDCodePostgreSQLFindErr = code.MustNewIDCode(code.PostgreSQLPrefix, 4)
	// IDCodePostgreSQLCountError ???
	IDCodePostgreSQLCountErr = code.MustNewIDCode(code.PostgreSQLPrefix, 5)
	// IDCodePostgreSQLUpdateError ???
	IDCodePostgreSQLUpdateErr = code.MustNewIDCode(code.PostgreSQLPrefix, 6)
	// IDCodePostgreSQLDeleteError ???
	IDCodePostgreSQLDeleteErr = code.MustNewIDCode(code.PostgreSQLPrefix, 7)
	// IDCodePostgreSQLCreateTxError ???
	IDCodePostgreSQLCreateTxErr = code.MustNewIDCode(code.PostgreSQLPrefix, 8)
	// IDCodePostgreSQLRollbackTxError ???
	IDCodePostgreSQLRollbackTxErr = code.MustNewIDCode(code.PostgreSQLPrefix, 9)
	// IDCodePostgreSQLCommitTxError ???
	IDCodePostgreSQLCommitTxErr = code.MustNewIDCode(code.PostgreSQLPrefix, 10)
	// IDCodePostgreSQLTruncateError ???
	IDCodePostgreSQLTruncateErr = code.MustNewIDCode(code.PostgreSQLPrefix, 11)
)
