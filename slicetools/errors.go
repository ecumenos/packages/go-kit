package slicetools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Slice Utils Codes
var (
	// IDCodeOnePickValueErr ???
	IDCodeOnePickValueErr = code.MustNewIDCode(code.SliceUtilsPrefix, 0)
	// IDCodeManyPickValuesErr ???
	IDCodeManyPickValuesErr = code.MustNewIDCode(code.SliceUtilsPrefix, 1)
	// IDCodeOnePickValueByNormErr ???
	IDCodeOnePickValueByNormErr = code.MustNewIDCode(code.SliceUtilsPrefix, 2)
	// IDCodeMeanIndGreaterTHanSliceErr ???
	IDCodeMeanIndGreaterTHanSliceErr = code.MustNewIDCode(code.SliceUtilsPrefix, 3)
)
