package slicetools

import (
	"context"
	"fmt"
	"math"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"gitlab.com/ecumenos/packages/go-kit/randomtools"
)

// RandomValueOfSlice ???
func RandomValueOfSlice[T any](ctx context.Context, randSrc func(float64) (float64, error), in []T) (T, error) {
	var zero T
	if len(in) == 0 {
		return zero, nil
	}
	if len(in) == 1 {
		return in[0], nil
	}

	r, err := randSrc(1)
	if err != nil {
		return zero, wrappederror.New(err, "Can not get random value from slice", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeOnePickValueErr)))
	}

	return in[int(math.Floor(r*float64(len(in))))], nil
}

// RandomValuesOfSlice ???
func RandomValuesOfSlice[T any](ctx context.Context, randSrc func(float64) (float64, error), in []T, amount int) ([]T, error) {
	if amount == 0 {
		return []T{}, nil
	}
	if len(in) <= amount {
		return in, nil
	}

	preOut := make(map[int]T)
	for {
		r, err := randSrc(1)
		if err != nil {
			return nil, wrappederror.New(err, "Can not get random value from slice", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeManyPickValuesErr)))
		}

		index := int(math.Floor(r * float64(len(in))))
		preOut[index] = in[index]
		if len(preOut) == amount {
			break
		}
	}

	out := make([]T, 0, amount)
	for _, v := range preOut {
		out = append(out, v)
	}

	return out, nil
}

// RandomValueOfSliceNorm ???
func RandomValueOfSliceNorm[T any](ctx context.Context, meanIndex float64, in []T) (T, error) {
	var zero T

	if meanIndex >= float64(len(in)) {
		return zero, wrappederror.New(nil, fmt.Sprintf("Can not get random value with norm (mean_index=%f, slice length=%d)", meanIndex, len(in)), wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeMeanIndGreaterTHanSliceErr)))
	}
	indexF, err := randomtools.GenFloat64NormInRange(ctx, 0, float64(len(in)-1), 1, float64(meanIndex))
	if err != nil {
		return zero, wrappederror.New(err, "Can not generate random index with norm", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeOnePickValueByNormErr)))
	}

	return in[int(indexF)], nil
}
