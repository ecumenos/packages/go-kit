package slicetools_test

import (
	"testing"

	"gitlab.com/ecumenos/packages/go-kit/slicetools"
)

func TestClone(t *testing.T) {
	s1 := []int{1, 2, 3}
	s2 := slicetools.Clone(s1)
	if !slicetools.Equal(s1, s2) {
		t.Errorf("Clone(%v) = %v, want %v", s1, s2, s1)
	}
	s1[0] = 4
	want := []int{1, 2, 3}
	if !slicetools.Equal(s2, want) {
		t.Errorf("Clone(%v) changed unexpectedly to %v", want, s2)
	}
	if got := slicetools.Clone([]int(nil)); got != nil {
		t.Errorf("Clone(nil) = %#v, want nil", got)
	}
	if got := slicetools.Clone(s1[:0]); got == nil || len(got) != 0 {
		t.Errorf("Clone(%v) = %#v, want %#v", s1[:0], got, s1[:0])
	}
}
