package timetools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Time Utils Codes
var (
	// IDCodeCastStringToTimeErr ???
	IDCodeCastStringToTimeErr = code.MustNewIDCode(code.TimeUtilsPrefix, 0)
)
