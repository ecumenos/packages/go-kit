package timetools

import (
	"context"
	"time"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// TimeToString serialize time.Time to string with default time format.
func TimeToString(in time.Time) string {
	return in.Format(DefaultTimeFormat)
}

// StringToTime deserialize string to time.Time with default time format.
func StringToTime(ctx context.Context, in string) (time.Time, error) {
	out, err := time.Parse(DefaultTimeFormat, in)
	if err != nil {
		return time.Time{}, wrappederror.New(err, "Can not convert string into datetime", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeCastStringToTimeErr)))
	}

	return out, nil
}
