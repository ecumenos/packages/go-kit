package testhelpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// AssertErrorMsg asserts that the error is not nil and that its message is
// equal to msg.  If msg is an empty string, AssertErrorMsg asserts that the
// error is nil instead.
func AssertErrorMsg(t testing.TB, msg string, err error) (ok bool) {
	t.Helper()

	if msg == "" {
		return assert.NoError(t, err)
	}

	if !assert.Error(t, err) {
		return false
	}

	return assert.Equal(t, msg, err.Error())
}
