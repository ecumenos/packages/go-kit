package httptools_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/httptools"
)

func TestCloneURL(t *testing.T) {
	t.Parallel()

	assert.Equal(t, (*url.URL)(nil), httptools.CloneURL(nil))
	assert.Equal(t, &url.URL{}, httptools.CloneURL(&url.URL{}))

	u, err := url.Parse("https://example.com/path?q=1&q=2#frag")
	require.NoError(t, err)

	clone := httptools.CloneURL(u)
	assert.Equal(t, u, clone)
	assert.NotSame(t, u, clone)
}

func TestJoinHostPort(t *testing.T) {
	t.Parallel()

	assert.Equal(t, ":0", httptools.JoinHostPort("", 0))
	assert.Equal(t, "host:12345", httptools.JoinHostPort("host", 12345))
	assert.Equal(t, "1.2.3.4:12345", httptools.JoinHostPort("1.2.3.4", 12345))
	assert.Equal(t, "[1234::5678]:12345", httptools.JoinHostPort("1234::5678", 12345))
	assert.Equal(t, "[1234::5678]:12345", httptools.JoinHostPort("[1234::5678]", 12345))
	assert.Equal(t, "[1234::5678%lo]:12345", httptools.JoinHostPort("1234::5678%lo", 12345))
}
