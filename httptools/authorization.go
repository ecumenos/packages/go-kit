package httptools

import (
	"net/http"
	"strings"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// ExtractJWTBearerToken returns bearer JWT token from request.
func ExtractJWTBearerToken(r *http.Request) (string, error) {
	ctx := r.Context()
	authHeader := r.Header.Get("Authorization")

	if authHeader == "" {
		return "", wrappederror.New(nil, "Authorization header missing", wrappederror.NewCodeOption(code.MustNewUnauthorized(ctx, IDCodeAuthorizationHeaderEmptyErr)))
	}

	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		return "", wrappederror.New(nil, "", wrappederror.NewCodeOption(code.MustNewUnauthorized(ctx, IDCodeIncorrectJWTTokenFormat)))
	}

	return authHeaderParts[1], nil
}
