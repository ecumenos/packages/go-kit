package httptools_test

import (
	"context"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"gitlab.com/ecumenos/packages/go-kit/httptools"
)

// Common test IPs.  Do not mutate.
var (
	testIPv4 = net.IP{1, 2, 3, 4}

	testIPv6 = net.IP{
		0x12, 0x34, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0xcd, 0xef,
	}
)

// Typed sinks for benchmarks.
var (
	errSink   error
	ipNetSink *net.IPNet
)

func TestCloneIPs(t *testing.T) {
	t.Parallel()

	assert.Equal(t, []net.IP(nil), httptools.CloneIPs(nil))
	assert.Equal(t, []net.IP{}, httptools.CloneIPs([]net.IP{}))

	ips := []net.IP{testIPv4}
	clone := httptools.CloneIPs(ips)
	assert.Equal(t, ips, clone)

	require.Len(t, clone, len(ips))
	require.Len(t, clone[0], len(ips[0]))

	assert.NotSame(t, &ips[0], &clone[0])
	assert.NotSame(t, &ips[0][0], &clone[0][0])
}

func TestSingleIPSubnet(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		want *net.IPNet
		name string
		in   net.IP
	}{{
		want: &net.IPNet{
			IP:   testIPv4,
			Mask: net.CIDRMask(32, 32),
		},
		name: "ipv4",
		in:   testIPv4,
	}, {
		want: &net.IPNet{
			IP:   testIPv6,
			Mask: net.CIDRMask(128, 128),
		},
		name: "ipv6",
		in:   testIPv6,
	}, {
		want: nil,
		name: "nil",
		in:   nil,
	}}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			got := httptools.SingleIPSubnet(tc.in)
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestSpecialAddrs(t *testing.T) {
	t.Parallel()

	assert.NotSame(t, httptools.IPv4bcast(), httptools.IPv4bcast())
	assert.NotSame(t, httptools.IPv4allsys(), httptools.IPv4allsys())
	assert.NotSame(t, httptools.IPv4allrouter(), httptools.IPv4allrouter())

	assert.NotSame(t, httptools.IPv4Zero(), httptools.IPv4Zero())
	assert.NotSame(t, httptools.IPv6Zero(), httptools.IPv6Zero())
}

func TestIPAndPortFromAddr(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		in       net.Addr
		wantIP   net.IP
		wantPort int
	}{{
		name:     "nil",
		in:       nil,
		wantIP:   nil,
		wantPort: 0,
	}, {
		name:     "tcp",
		in:       &net.TCPAddr{IP: testIPv4, Port: 12345},
		wantIP:   testIPv4,
		wantPort: 12345,
	}, {
		name:     "udp",
		in:       &net.UDPAddr{IP: testIPv4, Port: 12345},
		wantIP:   testIPv4,
		wantPort: 12345,
	}, {
		name:     "custom",
		in:       struct{ net.Addr }{},
		wantIP:   nil,
		wantPort: 0,
	}}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			gotIP, gotPort := httptools.IPAndPortFromAddr(tc.in)
			assert.Equal(t, tc.wantIP, gotIP)
			assert.Equal(t, tc.wantPort, gotPort)
		})
	}
}

func TestCloneIPNet(t *testing.T) {
	t.Parallel()

	var (
		mask4 = net.CIDRMask(16, httptools.IPv4BitLen)
		mask6 = net.CIDRMask(32, httptools.IPv6BitLen)
	)

	testCases := []struct {
		n    *net.IPNet
		name string
	}{{
		n:    &net.IPNet{IP: testIPv4, Mask: mask4},
		name: "common_v4",
	}, {
		n:    &net.IPNet{IP: nil, Mask: mask4},
		name: "nil_ip_v4",
	}, {
		n:    &net.IPNet{IP: testIPv4, Mask: nil},
		name: "nil_mask_v4",
	}, {
		n:    &net.IPNet{IP: testIPv6, Mask: mask6},
		name: "common_v6",
	}, {
		n:    &net.IPNet{IP: nil, Mask: mask6},
		name: "nil_ip_v6",
	}, {
		n:    &net.IPNet{IP: testIPv6, Mask: nil},
		name: "nil_mask_v6",
	}, {
		n:    &net.IPNet{IP: nil, Mask: nil},
		name: "empty",
	}, {
		n:    nil,
		name: "nil",
	}}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			clone := httptools.CloneIPNet(tc.n)
			assert.Equal(t, tc.n, clone)

			if tc.n == nil {
				return
			}

			assert.Len(t, clone.IP, len(tc.n.IP))
			assert.Len(t, clone.Mask, len(tc.n.Mask))

			assert.NotSame(t, tc.n, clone)
			if tc.n.IP != nil {
				assert.NotSame(t, &tc.n.IP[0], &clone.IP[0])
			}
			if tc.n.Mask != nil {
				assert.NotSame(t, &tc.n.Mask[0], &clone.Mask[0])
			}
		})
	}
}

func TestParseSubnet(t *testing.T) {
	t.Parallel()
	ctx := context.Background()

	testCases := []struct {
		want       *net.IPNet
		wantErrMsg string
		name       string
		in         string
	}{{
		want:       httptools.SingleIPSubnet(testIPv4),
		wantErrMsg: "",
		name:       "success_ipv4",
		in:         "1.2.3.4",
	}, {
		want:       httptools.SingleIPSubnet(testIPv6),
		wantErrMsg: "",
		name:       "success_ipv6",
		in:         "1234::cdef",
	}, {
		want:       &net.IPNet{IP: testIPv6, Mask: net.CIDRMask(16, httptools.IPv6BitLen)},
		wantErrMsg: "",
		name:       "success_ipv6",
		in:         "1234::cdef/16",
	}, {
		want:       nil,
		wantErrMsg: "Can not parse subnet (subnet=1.2.3.4.5)",
		name:       "bad_ipv4",
		in:         "1.2.3.4.5",
	}, {
		want:       nil,
		wantErrMsg: "Can not parse subnet (subnet=1234:::cdef)",
		name:       "bad_ipv6",
		in:         "1234:::cdef",
	}, {
		want:       nil,
		wantErrMsg: "Can not parse subnet (subnet=1.2.3.4//16)",
		name:       "bad_cidr",
		in:         "1.2.3.4//16",
	}, {
		want:       &net.IPNet{IP: testIPv4, Mask: net.CIDRMask(0, httptools.IPv4BitLen)},
		wantErrMsg: "",
		name:       "success_4_to_6",
		in:         "::ffff:1.2.3.4/96",
	}, {
		want:       &net.IPNet{IP: testIPv4, Mask: net.CIDRMask(16, httptools.IPv6BitLen)},
		wantErrMsg: "",
		name:       "success_not_4_to_6",
		in:         "::ffff:1.2.3.4/16",
	}}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			n, err := httptools.ParseSubnet(ctx, tc.in)
			assert.Equal(t, tc.want, n)
			if err == nil && tc.wantErrMsg != "" {
				t.Fail()
			}
			if err != nil && tc.wantErrMsg == "" {
				t.Fail()
			}
			if err != nil {
				assert.ErrorAs(t, err, new(*wrappederror.Wrapped))
			}
		})
	}
}

func TestParseSubnet_equivalence(t *testing.T) {
	t.Parallel()
	ctx := context.Background()

	const netStr = "::ffff:1.2.3.4/16"

	parsedIP, netN, err := net.ParseCIDR(netStr)
	require.NoError(t, err)

	httptoolsN, err := httptools.ParseSubnet(ctx, netStr)
	require.NoError(t, err)

	testCases := map[string]struct {
		want assert.ComparisonAssertionFunc
		ip   net.IP
	}{
		"ip4": {
			want: assert.NotEqual,
			ip:   testIPv4,
		},
		"ip6": {
			want: assert.Equal,
			ip:   testIPv6,
		},
		"ip4_zero": {
			want: assert.NotEqual,
			ip:   httptools.IPv4Zero(),
		},
		"ip6_zero": {
			want: assert.NotEqual,
			ip:   httptools.IPv6Zero(),
		},
		"ip_from_str": {
			want: assert.NotEqual,
			ip:   parsedIP,
		},
		"invalid": {
			want: assert.Equal,
			ip:   net.IP{1, 2, 3, 4, 5},
		},
		"nil": {
			want: assert.Equal,
			ip:   nil,
		}}

	for name, tc := range testCases {
		tc := tc
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tc.want(t, netN.Contains(tc.ip), httptoolsN.Contains(tc.ip))
		})
	}
}

func TestValidateIP(t *testing.T) {
	t.Parallel()
	ctx := context.Background()

	testCases := map[string]struct {
		wantErrMsg string
		in         net.IP
	}{
		"success_ipv4": {
			wantErrMsg: "",
			in:         testIPv4,
		},
		"success_ipv6": {
			wantErrMsg: "",
			in:         testIPv6,
		},
		"error_nil": {
			wantErrMsg: "Incorrect IP address length",
			in:         nil,
		},
		"error_empty": {
			wantErrMsg: "Incorrect IP address length",
			in:         net.IP{},
		},
		"error_bad": {
			wantErrMsg: "Incorrect IP address length",
			in:         net.IP{1, 2, 3},
		}}

	for name, tc := range testCases {
		tc := tc
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			err := httptools.ValidateIP(ctx, tc.in)
			if err == nil && tc.wantErrMsg != "" {
				t.Fail()
			}
			if err != nil && tc.wantErrMsg == "" {
				t.Fail()
			}
		})
	}
}

func BenchmarkParseSubnet(b *testing.B) {
	ctx := context.Background()

	benchCases := map[string]struct {
		in string
	}{
		"good_cidr4": {
			in: "1.2.3.4/16",
		},
		"good_ip4": {
			in: "1.2.3.4",
		},
		"good_cidr6": {
			in: "abcd::1234/96",
		},
		"good_ip6": {
			in: "abcd::1234",
		},
		"good_cidr4to6": {
			in: "::ffff:1.2.3.4/97",
		},
		"good_ip4to6": {
			in: "::ffff:1.2.3.4",
		},
		"good_cidr_not4to6": {
			in: "::ffff:1.2.3.4/16",
		}}

	for name, bc := range benchCases {
		b.Run(name, func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()
			for i := 0; i < b.N; i++ {
				ipNetSink, errSink = httptools.ParseSubnet(ctx, bc.in)
			}

			require.NotNil(b, ipNetSink)
			require.NoError(b, errSink)
		})
	}

	benchErrCases := map[string]struct {
		in string
	}{
		"bad_cidr": {
			in: "1.2.3.4//567",
		},
		"bad_ip": {
			in: "1.2.3.4.5",
		}}

	for name, bc := range benchErrCases {
		b.Run(name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				_, errSink = httptools.ParseSubnet(ctx, bc.in)
			}

			require.Error(b, errSink)
		})
	}
}
