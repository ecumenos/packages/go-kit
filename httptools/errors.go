package httptools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// HTTP Utils Codes
var (
	// IDCodeExtractIPFromRemoteAddrErr ???
	IDCodeExtractIPFromRemoteAddrErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 0)
	// IDCodeSplitRemoteAddrErr ???
	IDCodeSplitRemoteAddrErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 1)
	// IDCodeParsePortErr ???
	IDCodeParsePortErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 2)
	// IDCodeParseIPErr ???
	IDCodeParseIPErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 3)
	// IDCodeParseIPv4Err ???
	IDCodeParseIPv4Err = code.MustNewIDCode(code.HTTPUtilsPrefix, 4)
	// IDCodeParseIPForParsingSubnetErr ???
	IDCodeParseIPForParsingSubnetErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 5)
	// IDCodeParseCIDRForParsingSubnetErr ???
	IDCodeParseCIDRForParsingSubnetErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 6)
	// IDCodeParseCIDRErr ???
	IDCodeParseCIDRErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 7)
	// IDCodeIncorrectIPLengthErr ???
	IDCodeIncorrectIPLengthErr = code.MustNewIDCode(code.HTTPUtilsPrefix, 8)
)

// Auth Codes
var (
	// IDCodeAuthorizationHeaderEmptyErr ???
	IDCodeAuthorizationHeaderEmptyErr = code.MustNewIDCode(code.AuthPrefix, 0)
	// IDCodeIncorrectJWTToken ???
	IDCodeIncorrectJWTToken = code.MustNewIDCode(code.AuthPrefix, 1)
	// IDCodeIncorrectJWTTokenFormat ???
	IDCodeIncorrectJWTTokenFormat = code.MustNewIDCode(code.AuthPrefix, 2)
	// IDCodeJWTTokenExpired ???
	IDCodeJWTTokenExpired = code.MustNewIDCode(code.AuthPrefix, 3)
	// IDCodeForbiddenForRole ???
	IDCodeForbiddenForRole = code.MustNewIDCode(code.AuthPrefix, 4)
)
