package wrappederror

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"runtime"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
)

// Wrapped ???
type Wrapped struct {
	Cause    error     `json:"cause"`
	Message  string    `json:"message"`
	Code     code.Code `json:"code"`
	FuncName string    `json:"func_name"`
}

// New ???
func New(cause error, msg string, opts ...Option) error {
	w := &Wrapped{
		Cause:   cause,
		Message: msg,
	}
	for _, opt := range opts {
		w = opt(w)
	}

	if pc, _, _, ok := runtime.Caller(1); ok {
		if fn := runtime.FuncForPC(pc); fn != nil {
			w.FuncName = fn.Name()
		}
	}

	return w
}

// ErrorMap ???
func (e *Wrapped) ErrorMap() map[string]interface{} {
	if e == nil {
		return nil
	}

	return map[string]interface{}{
		"message":   e.Message,
		"cause":     Cast(e.Cause).ErrorMap(),
		"code":      e.Code.ComplexCode(),
		"func_name": e.FuncName,
	}
}

// Error ???
func (e *Wrapped) Error() string {
	if e == nil {
		return "<nil>"
	}

	cause := "<nil>"
	if e.Cause != nil {
		cause = e.Cause.Error()
	}

	b, err := json.Marshal(e.ErrorMap())
	if err != nil {
		return fmt.Sprintf("cause: %s, message: [%s] %s, code: %d", cause, e.FuncName, e.Message, e.Code.ComplexCode())
	}

	return string(b)
}

// CastStrict cases error into *Wrapped. If input is not *Wrapped it will returns <nil>.
func CastStrict(err error) *Wrapped {
	if err == nil {
		return nil
	}

	var ce *Wrapped
	if errors.As(err, &ce) {
		return ce
	}

	return nil
}

// Cast cases error into *Wrapped. If input is not *Wrapped it will creates new *Wrapped.
func Cast(err error) *Wrapped {
	if err == nil {
		return nil
	}

	if ce := CastStrict(err); ce != nil {
		return ce
	}
	c, _ := code.New(0, http.StatusInternalServerError, code.GRPCStatusCodeInternal, 0)

	return &Wrapped{
		Cause:   err,
		Message: "something went wrong",
		Code:    c,
	}
}

// Equals check if input values are equals
func Equals(left, right error) bool {
	if left == nil || right == nil {
		return errors.Is(left, right)
	}

	return left.Error() == right.Error()
}
