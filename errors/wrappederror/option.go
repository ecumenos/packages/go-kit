package wrappederror

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Option ???
type Option func(*Wrapped) *Wrapped

// NewCodeOption ???
func NewCodeOption(c code.Code) Option {
	return func(w *Wrapped) *Wrapped {
		w.Code = c

		return w
	}
}
