package wrappederror

import (
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
)

func TestCast(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	errMsg := "error message"
	w := Cast(errors.New(errMsg))
	require.NotNil(w)
	assert.Equal(errMsg, w.Cause.Error())
	assert.Equal("something went wrong", w.Message)
	c, err := code.New(0, http.StatusInternalServerError, code.GRPCStatusCodeInternal, 0)
	require.NoError(err)
	assert.Equal(c.ComplexCode(), w.Code.ComplexCode())

	var id code.IDCode = 777
	c, err = code.New(code.AreaCodePeerNode, http.StatusNotFound, code.GRPCStatusCodeNotFound, id)
	require.NoError(err)
	w = Cast(New(nil, errMsg, NewCodeOption(c)))
	require.NotNil(w)
	assert.Equal(nil, w.Cause)
	assert.Equal(c.ComplexCode(), w.Code.ComplexCode())
	assert.Equal(errMsg, w.Message)
}

func TestCastStrict(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	errMsg := "error message"
	w := CastStrict(errors.New(errMsg))
	require.Nil(w)

	var id code.IDCode = 777
	c, err := code.New(code.AreaCodePeerNode, http.StatusNotFound, code.GRPCStatusCodeNotFound, id)
	require.NoError(err)
	w = Cast(New(nil, errMsg, NewCodeOption(c)))
	require.NotNil(w)
	assert.Equal(nil, w.Cause)
	assert.Equal(c.ComplexCode(), w.Code.ComplexCode())
	assert.Equal(errMsg, w.Message)
}

func TestEquals(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	errMsg := "error message"
	assert.True(Equals(errors.New(errMsg), errors.New(errMsg)))
	assert.False(Equals(errors.New(errMsg), nil))
	assert.False(Equals(nil, errors.New(errMsg)))
	assert.True(Equals(nil, nil))

	var id code.IDCode = 777
	c1, err := code.New(code.AreaCodePeerNode, http.StatusNotFound, code.GRPCStatusCodeNotFound, id)
	require.NoError(err)
	c2, err := code.New(code.AreaCodePeerNode, http.StatusInternalServerError, code.GRPCStatusCodeInternal, id)
	require.NoError(err)
	assert.True(Equals(New(nil, errMsg, NewCodeOption(c1)), New(nil, errMsg, NewCodeOption(c1))))
	assert.False(Equals(New(nil, errMsg, NewCodeOption(c1)), New(nil, errMsg+"1", NewCodeOption(c1))))
	assert.False(Equals(New(nil, errMsg, NewCodeOption(c1)), New(nil, errMsg, NewCodeOption(c2))))
}
