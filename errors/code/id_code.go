package code

import (
	"errors"
	"fmt"
	"math"
)

// IDCode ???
type IDCode uint

// MustNewIDCode ???
func MustNewIDCode(prefix IDCodePrefix, id uint16) IDCode {
	out, err := NewIDCode(prefix, id)
	if err != nil {
		panic(err)
	}

	return out
}

// NewIDCode ???
func NewIDCode(prefix IDCodePrefix, id uint16) (IDCode, error) {
	if err := prefix.Validate(); err != nil {
		return 0, err
	}
	if id > 99 {
		return 0, errors.New("can not create code if code is greater than 99")
	}

	var out IDCode
	out += IDCode(prefix.UInt64()) * 100
	out += IDCode(id)

	return out, nil
}

// UInt64 ???
func (c IDCode) UInt64() uint64 {
	return uint64(c)
}

// Validate ???
func (c IDCode) Validate() error {
	if c >= IDCode(math.Pow10(idWidth)) {
		return fmt.Errorf("ID code can not greater or equal than %d", int(math.Pow10(idWidth)))
	}

	return nil
}
