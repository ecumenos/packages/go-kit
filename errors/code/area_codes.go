package code

import (
	"context"
	"errors"

	"gitlab.com/ecumenos/packages/go-kit/contexttools"
)

// AreaCode ???
type AreaCode int

// UInt64 ???
func (c AreaCode) UInt64() uint64 {
	return uint64(c)
}

// Validate ???
func (c AreaCode) Validate() error {
	if c < 0 {
		return errors.New("area code can not negative number")
	}
	if c > 999 {
		return errors.New("area code can not greater than 999")
	}

	return nil
}

const (
	// AreaCodePublicNode ???
	AreaCodePublicNode = AreaCode(100)
	// AreaCodePeerNode ???
	AreaCodePeerNode = AreaCode(101)
	// AreaCodeIdentityService ???
	AreaCodeIdentityService = AreaCode(102)
	// AreaCodeNameService ???
	AreaCodeNameService = AreaCode(103)
	// AreaCodeUnknownService ???
	AreaCodeUnknownService = AreaCode(999)
)

var areaNames = map[AreaCode]string{
	AreaCodePublicNode:      "Public Node",
	AreaCodePeerNode:        "Peer Node",
	AreaCodeIdentityService: "Identity Service",
	AreaCodeNameService:     "Name Service",
	AreaCodeUnknownService:  "",
}

// GetAreaCodeByServiceName ???
func GetAreaCodeByServiceName(name string) AreaCode {
	for key, value := range areaNames {
		if value == name {
			return key
		}
	}

	return AreaCodeUnknownService
}

// GetAreaCodeByContext ???
func GetAreaCodeByContext(ctx context.Context) AreaCode {
	name := contexttools.GetValue(ctx, contexttools.ServiceNameKey)

	return GetAreaCodeByServiceName(name)
}
