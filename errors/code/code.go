package code

import (
	"fmt"
	"math"
	"net/http"
)

// Code represents code
type Code interface {
	ComplexCode() uint64
	HTTPStatusCode() int
	GRPCStatusCode() GRPCStatusCode
	AreaCode() AreaCode
	AreaName() string
	ID() IDCode
}

// Impl ???
type Impl uint64

const (
	areaCodePosition       = 9
	httpStatusCodePosition = 6
	grpcStatusCodePosition = 4
	idPosition             = 0

	areaCodeWidth       = 3
	httpStatusCodeWidth = 3
	grpcStatusCodeWidth = 2
	idWidth             = 4
)

// New ???
// Code contains: xxx(area_code)xxx(http_code)xx(grpc_code)xxxx(id)
func New(areaCode AreaCode, httpStatusCode int, grpcStatusCode GRPCStatusCode, idCode IDCode) (Code, error) {
	if err := areaCode.Validate(); err != nil {
		return nil, err
	}
	if statusText := http.StatusText(httpStatusCode); statusText == "" {
		return nil, fmt.Errorf("%d is invalid HTTP status code", httpStatusCode)
	}
	if err := grpcStatusCode.Validate(); err != nil {
		return nil, err
	}
	if err := idCode.Validate(); err != nil {
		return nil, err
	}

	var c uint64
	c += idCode.UInt64() * uint64(math.Pow10(idPosition))
	c += grpcStatusCode.UInt64() * uint64(math.Pow10(grpcStatusCodePosition))
	c += uint64(httpStatusCode) * uint64(math.Pow10(httpStatusCodePosition))
	c += areaCode.UInt64() * uint64(math.Pow10(areaCodePosition))

	return Impl(c), nil
}

// MustNew ???
func MustNew(areaCode AreaCode, httpStatusCode int, grpcStatusCode GRPCStatusCode, idCode IDCode) Code {
	c, err := New(areaCode, httpStatusCode, grpcStatusCode, idCode)
	if err != nil {
		panic(err)
	}

	return c
}

// UInt64 ???
func (c Impl) UInt64() uint64 {
	return uint64(c)
}

// ComplexCode ???
func (c Impl) ComplexCode() uint64 {
	return c.UInt64()
}

// HTTPStatusCode ???
func (c Impl) HTTPStatusCode() int {
	return c.extractCodeByWidth(httpStatusCodePosition, httpStatusCodeWidth)
}

// GRPCStatusCode ???
func (c Impl) GRPCStatusCode() GRPCStatusCode {
	return GRPCStatusCode(c.extractCodeByWidth(grpcStatusCodePosition, grpcStatusCodeWidth))
}

// AreaCode ???
func (c Impl) AreaCode() AreaCode {
	return AreaCode(c.extractCodeByWidth(areaCodePosition, areaCodeWidth))
}

// AreaName ???
func (c Impl) AreaName() string {
	areaCode := AreaCode(c.extractCodeByWidth(areaCodePosition, areaCodeWidth))
	name, ok := areaNames[areaCode]
	if !ok {
		return ""
	}

	return name
}

// ID ???
func (c Impl) ID() IDCode {
	return IDCode(c.extractCodeByWidth(idPosition, idWidth))
}

func (c Impl) extractCodeByWidth(rightDigitPosShift, width int) int {
	wc := c.UInt64()

	digitShiftMagnitude := uint64(math.Pow10(rightDigitPosShift))
	wc /= digitShiftMagnitude

	modDivisor := int(math.Pow10(width))

	return int(wc) % modDivisor
}
