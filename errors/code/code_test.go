package code_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
)

func Test_Code(t *testing.T) {
	t.Run("should work as expected for all 0's", func(t *testing.T) {
		c := code.Impl(0)

		require.Equal(t, uint64(0), c.UInt64())
		assert.Equal(t, code.AreaCode(0), c.AreaCode())
		assert.Equal(t, int(0), c.HTTPStatusCode())
		assert.Equal(t, code.GRPCStatusCode(0), c.GRPCStatusCode())
		assert.Equal(t, code.IDCode(0), c.ID())
	})
	t.Run("should work as expected for completely unique digits", func(t *testing.T) {
		c := code.Impl(134_567_12_0890)

		require.Equal(t, uint64(134_567_12_0890), c.UInt64())
		assert.Equal(t, code.AreaCode(134), c.AreaCode())
		assert.Equal(t, code.GRPCStatusCode(12), c.GRPCStatusCode())
		assert.Equal(t, code.IDCode(890), c.ID())
	})
}

func Test_New(t *testing.T) {
	t.Run("should work for completely unique digits", func(t *testing.T) {
		c, err := code.New(345, 200, 12, 890)
		require.NoError(t, err)
		require.Equal(t, uint64(345_200_12_0890), c.ComplexCode())
	})
	t.Run("should work for all zeros (lower boundary)", func(t *testing.T) {
		c, err := code.New(0, http.StatusOK, 0, 0)
		require.NoError(t, err)
		require.Equal(t, uint64(200000000), c.ComplexCode())
	})
	t.Run("should work for max values in every field (upper boundary)", func(t *testing.T) {
		c, err := code.New(999, 400, 16, 9999)
		require.NoError(t, err)
		require.Equal(t, uint64(999_400_16_9999), c.ComplexCode())
	})
	t.Run("should fail for invalid inputs", func(t *testing.T) {
		var err error
		_, err = code.New(-1, 0, 0, 0)
		assert.Error(t, err)

		_, err = code.New(0, -1, 0, 0)
		require.Error(t, err)

		_, err = code.New(0, 0, -1, 0)
		require.Error(t, err)

		_, err = code.New(1000, 0, 0, 0)
		require.Error(t, err)

		_, err = code.New(0, 99, 0, 0)
		require.Error(t, err)

		_, err = code.New(0, 0, 17, 0)
		require.Error(t, err)

		_, err = code.New(0, 0, 0, 10000)
		require.Error(t, err)
	})
}
