package code

import "errors"

// IDCodePrefix ???
type IDCodePrefix uint8

// UInt64 ???
func (p IDCodePrefix) UInt64() uint64 {
	return uint64(p)
}

// Validate ???
func (p IDCodePrefix) Validate() error {
	if p > 99 {
		return errors.New("id code prefix can not be greater than 99")
	}

	return nil
}

const (
	// DefaultPrefix ???
	DefaultPrefix IDCodePrefix = 10
	// CryptoPrefix ???
	CryptoPrefix IDCodePrefix = 12
	// HTTPUtilsPrefix ???
	HTTPUtilsPrefix IDCodePrefix = 13
	// MapUtilsPrefix ???
	MapUtilsPrefix IDCodePrefix = 14
	// NumberUtilsPrefix ???
	NumberUtilsPrefix IDCodePrefix = 15
	// RandomUtilsPrefix ???
	RandomUtilsPrefix IDCodePrefix = 16
	// SliceUtilsPrefix ???
	SliceUtilsPrefix IDCodePrefix = 17
	// StringUtilsPrefix ???
	StringUtilsPrefix IDCodePrefix = 18
	// TimeUtilsPrefix ???
	TimeUtilsPrefix IDCodePrefix = 19
	// AuthPrefix ???
	AuthPrefix IDCodePrefix = 20
	// PostgreSQLPrefix ???
	PostgreSQLPrefix IDCodePrefix = 21
	// MongoDBPrefix ???
	MongoDBPrefix IDCodePrefix = 22
	// RedisPrefix ???
	RedisPrefix IDCodePrefix = 24
	// RequestProcessingPrefix ???
	RequestProcessingPrefix IDCodePrefix = 25
	// ServerInternalPrefix ???
	ServerInternalPrefix IDCodePrefix = 25
)
