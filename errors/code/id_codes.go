package code

// Codes
var (
	IDCodeDefault                   = MustNewIDCode(DefaultPrefix, 0)
	IDCodeRequestBodyValidationErr  = MustNewIDCode(ServerInternalPrefix, 0)
	IDCodePathParamsValidationErr   = MustNewIDCode(ServerInternalPrefix, 1)
	IDCodeQueryParamsValidationErr  = MustNewIDCode(ServerInternalPrefix, 2)
	IDCodeGRPCRequestValidationErr  = MustNewIDCode(ServerInternalPrefix, 3)
	IDCodeResponseBodyValidationErr = MustNewIDCode(ServerInternalPrefix, 4)
	IDCodeEndpointNotImplemented    = MustNewIDCode(ServerInternalPrefix, 5)
)
