package code

import (
	"context"
	"net/http"
)

// NewMustFactory ???
func NewMustFactory(httpStatusCode int, grpcStatusCode GRPCStatusCode, idCode IDCode) func(context.Context) Code {
	return func(ctx context.Context) Code {
		return MustNew(GetAreaCodeByContext(ctx), httpStatusCode, grpcStatusCode, idCode)
	}
}

// MustNewWithContext ???
func MustNewWithContext(ctx context.Context, httpStatusCode int, grpcStatusCode GRPCStatusCode, idCode IDCode) Code {
	return MustNew(GetAreaCodeByContext(ctx), httpStatusCode, grpcStatusCode, idCode)
}

// MustNewBadRequest ???
func MustNewBadRequest(ctx context.Context, idCode IDCode) Code {
	return MustNewWithContext(ctx, http.StatusBadRequest, GRPCStatusCodeInvalidArgument, idCode)
}

// MustNewUnauthorized ???
func MustNewUnauthorized(ctx context.Context, idCode IDCode) Code {
	return MustNewWithContext(ctx, http.StatusUnauthorized, GRPCStatusCodeUnauthenticated, idCode)
}

// MustNewForbidden ???
func MustNewForbidden(ctx context.Context, idCode IDCode) Code {
	return MustNewWithContext(ctx, http.StatusForbidden, GRPCStatusCodeUnauthenticated, idCode)
}

// MustNewNotFound ???
func MustNewNotFound(ctx context.Context, idCode IDCode) Code {
	return MustNewWithContext(ctx, http.StatusNotFound, GRPCStatusCodeNotFound, idCode)
}

// MustNewInternalError ???
func MustNewInternalError(ctx context.Context, idCode IDCode) Code {
	return MustNewWithContext(ctx, http.StatusInternalServerError, GRPCStatusCodeInternal, idCode)
}
