package contexttools

// Key is custom type for key of value that is in context.Context.
type Key string
