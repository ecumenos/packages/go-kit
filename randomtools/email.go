package randomtools

import (
	"context"
	"fmt"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// GenEmail generate random email.
func GenEmail(ctx context.Context) (string, error) {
	username, err := GenString(ctx, 100)
	if err != nil {
		return "", wrappederror.New(err, "Can not random username for email", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenUsernameForEmailErr)))
	}

	domainName, err := GenString(ctx, 5)
	if err != nil {
		return "", wrappederror.New(err, "Can not random domain name for email", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenDomainNameForEmailErr)))
	}

	extension, err := GenString(ctx, 3)
	if err != nil {
		return "", wrappederror.New(err, "Can not random extension for email", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenExtensionForEmailErr)))
	}

	return fmt.Sprintf("%s@%s.%s", username, domainName, extension), nil
}

// GenEmails generate random emails.
func GenEmails(ctx context.Context, len int) ([]string, error) {
	emails := make([]string, len)

	for i := 0; i < len; i++ {
		email, err := GenEmail(ctx)
		if err != nil {
			return nil, wrappederror.New(err, "Can not random email for emails generation", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenEmailErr)))
		}
		emails[i] = email
	}

	return emails, nil
}
