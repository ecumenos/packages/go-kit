package randomtools

import (
	"context"
	"crypto/rand"
	"math/big"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// GenIntInRange ???
func GenIntInRange(ctx context.Context, min int, max int) (int, error) {
	if min >= max {
		return 0, wrappederror.New(nil, "Can not generate integer for incorrect min max parameters", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenIntIfMinIsGreaterThanMaxErr)))
	}

	n, err := rand.Int(rand.Reader, big.NewInt(int64(max-min)))
	if err != nil {
		return 0, wrappederror.New(err, "Can not generate integer", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenIntErr)))
	}

	return int(int64(min) + n.Int64()), nil
}

// GenInt ???
func GenInt(ctx context.Context, max int) (int, error) {
	out, err := GenIntInRange(ctx, 0, max)
	if err != nil {
		return 0, err
	}

	return out, nil
}
