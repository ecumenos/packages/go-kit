package randomtools_test

import (
	"context"
	"fmt"

	"gitlab.com/ecumenos/packages/go-kit/randomtools"
)

func ExampleGenBool() {
	result, _ := randomtools.GenBool(context.Background(), 1)
	fmt.Print(result)
	// Output:
	// true
}
