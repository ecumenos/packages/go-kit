package randomtools

import (
	"context"

	goNanoID "github.com/matoous/go-nanoid"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// GenNanoString generate nano string
func GenNanoString(ctx context.Context, length int) (string, error) {
	id, err := goNanoID.ID(length)
	if err != nil {
		return "", wrappederror.New(err, "Can not generate string", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenNanoStrErr)))
	}

	return id, nil
}
