package randomtools

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	expRand "golang.org/x/exp/rand"
	"gonum.org/v1/gonum/stat/distuv"
)

// GenFloat64InRange ???
func GenFloat64InRange(ctx context.Context, min, max float64) (float64, error) {
	if min >= max {
		return 0, wrappederror.New(nil, "Can not generate float64 for incorrect min max parameters", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenFloat64IfMinIsGreaterThanMaxErr)))
	}

	rInt, err := GenIntInRange(ctx, 0, 100)
	if err != nil {
		return 0, wrappederror.New(err, "Can not generate float64", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenIntForFloat64Err)))
	}
	s := expRand.NewSource(uint64(rInt))

	return min + expRand.New(s).Float64()*(max-min), nil
}

// GenFloat64 ???
func GenFloat64(ctx context.Context, max float64) (float64, error) {
	out, err := GenFloat64InRange(ctx, 0, max)
	if err != nil {
		return 0, wrappederror.New(err, "Can not generate float64", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenFloat64FromZeroFoMaxErr)))
	}

	return out, nil
}

// GenFloat64Norm generate random float64 with norm
//
// stdDev -standart deviation - σ^2; default = 1
//
// mean - μ (In probability theory, the expected value is a generalization of the weighted average.); default = 0
func GenFloat64Norm(ctx context.Context, stdDev, mean float64) (float64, error) {
	rInt, err := GenIntInRange(ctx, 0, 100)
	if err != nil {
		return 0, wrappederror.New(err, "Can not generate float64 for normal distribution", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenIntForFloat64NormErr)))
	}
	s := expRand.NewSource(uint64(rInt))

	dist := distuv.Normal{
		Mu:    mean,   // Mean of the normal distribution
		Sigma: stdDev, // Standard deviation of the normal distribution
		Src:   s,
	}

	return dist.Rand(), nil
}

// GenFloat64NormInRange ???
func GenFloat64NormInRange(ctx context.Context, min, max, stdDev, mean float64) (float64, error) {
	if min >= max {
		return 0, wrappederror.New(nil, "Can not generate float64 for normal distribution for incorrect min max parameters", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenFloat64NormIfMinIsGreaterThanMaxErr)))
	}

	out, err := genFloat64NormInRange(ctx, min, max, stdDev, mean, 10)
	if err != nil {
		return 0, err
	}

	return out, nil
}

// genFloat64NormInRange ???
func genFloat64NormInRange(ctx context.Context, min, max, stdDev, mean float64, count int) (float64, error) {
	var multiplier float64 = 10

	min *= multiplier
	max *= multiplier
	mean *= multiplier

	r, err := GenFloat64Norm(ctx, stdDev, mean)
	if err != nil {
		return 0, err
	}

	if r < min {
		if count == 0 {
			return min / multiplier, nil
		}
		return genFloat64NormInRange(ctx, min/multiplier, max/multiplier, stdDev, mean/multiplier, count-1)
	}
	if r > max {
		if count == 0 {
			return max / multiplier, nil
		}
		return genFloat64NormInRange(ctx, min/multiplier, max/multiplier, stdDev, mean/multiplier, count-1)
	}

	return r / multiplier, nil
}
