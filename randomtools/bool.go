package randomtools

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// GenBool generate bool by probability of true response.
func GenBool(ctx context.Context, trueProb float64) (bool, error) {
	n, err := GenFloat64(ctx, 1)
	if err != nil {
		return false, wrappederror.New(err, "Can not generate random float64 for generation boolean", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenFloatForBoolErr)))
	}

	return n < trueProb, nil
}
