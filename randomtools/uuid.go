package randomtools

import (
	"github.com/google/uuid"
)

// GenUUID generate uuid.GenUUID.
func GenUUID() uuid.UUID {
	return uuid.New()
}

// GenUUIDString generate UUID string.
func GenUUIDString() string {
	return uuid.NewString()
}
