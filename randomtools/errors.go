package randomtools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Random Utils Codes
var (
	// IDCodeGenFloatForBoolErr ???
	IDCodeGenFloatForBoolErr = code.MustNewIDCode(code.RandomUtilsPrefix, 0)
	// IDCodeGenNanoStrErr ???
	IDCodeGenNanoStrErr = code.MustNewIDCode(code.RandomUtilsPrefix, 1)
	// IDCodeGenEmailErr ???
	IDCodeGenEmailErr = code.MustNewIDCode(code.RandomUtilsPrefix, 10)
	// IDCodeGenUsernameForEmailErr ???
	IDCodeGenUsernameForEmailErr = code.MustNewIDCode(code.RandomUtilsPrefix, 11)
	// IDCodeGenDomainNameForEmailErr ???
	IDCodeGenDomainNameForEmailErr = code.MustNewIDCode(code.RandomUtilsPrefix, 12)
	// IDCodeGenExtensionForEmailErr ???
	IDCodeGenExtensionForEmailErr = code.MustNewIDCode(code.RandomUtilsPrefix, 13)
	// IDCodeGenFloat64IfMinIsGreaterThanMaxErr ???
	IDCodeGenFloat64IfMinIsGreaterThanMaxErr = code.MustNewIDCode(code.RandomUtilsPrefix, 20)
	// IDCodeGenIntForFloat64Err ???
	IDCodeGenIntForFloat64Err = code.MustNewIDCode(code.RandomUtilsPrefix, 21)
	// IDCodeGenFloat64FromZeroFoMaxErr ???
	IDCodeGenFloat64FromZeroFoMaxErr = code.MustNewIDCode(code.RandomUtilsPrefix, 22)
	// IDCodeGenIntForFloat64NormErr ???
	IDCodeGenIntForFloat64NormErr = code.MustNewIDCode(code.RandomUtilsPrefix, 23)
	// IDCodeGenFloat64NormIfMinIsGreaterThanMaxErr ???
	IDCodeGenFloat64NormIfMinIsGreaterThanMaxErr = code.MustNewIDCode(code.RandomUtilsPrefix, 24)
	// IDCodeGenIntIfMinIsGreaterThanMaxErr ???
	IDCodeGenIntIfMinIsGreaterThanMaxErr = code.MustNewIDCode(code.RandomUtilsPrefix, 25)
	// IDCodeGenIntErr ???
	IDCodeGenIntErr = code.MustNewIDCode(code.RandomUtilsPrefix, 26)
	// IDCodeGenIntForStrErr ???
	IDCodeGenIntForStrErr = code.MustNewIDCode(code.RandomUtilsPrefix, 27)
)
