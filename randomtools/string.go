package randomtools

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

const (
	asciiFirstLowcaseLetterCode = 97
	asciiLastLowcaseLetterCode  = 122
)

// GenString generate random string.
func GenString(ctx context.Context, len int) (string, error) {
	bytes := make([]byte, len)

	for i := 0; i < len; i++ {
		n, err := GenIntInRange(ctx, asciiFirstLowcaseLetterCode, asciiLastLowcaseLetterCode)
		if err != nil {
			return "", wrappederror.New(err, "Can not generate random string", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeGenIntForStrErr)))
		}
		bytes[i] = byte(n)
	}

	return string(bytes), nil
}
