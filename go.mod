module gitlab.com/ecumenos/packages/go-kit

go 1.20

require (
	github.com/blang/semver/v4 v4.0.0
	github.com/google/uuid v1.3.0
	github.com/matoous/go-nanoid v1.5.0
	github.com/stretchr/testify v1.8.2
	golang.org/x/crypto v0.8.0
	golang.org/x/exp v0.0.0-20230425010034-47ecfdc1ba53
	golang.org/x/text v0.9.0
	gonum.org/v1/gonum v0.12.0
	google.golang.org/grpc v1.54.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
