package version

import (
	"context"
	"fmt"

	"github.com/blang/semver/v4"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"gitlab.com/ecumenos/packages/go-kit/stringtools"
)

// ParseSemverVersion returns parsed semver.Version.
func ParseSemverVersion(ctx context.Context, v string) (semver.Version, error) {
	out, err := semver.Parse(v)
	if err != nil {
		return semver.Version{}, wrappederror.New(err, "[ParseVersion] can not parse version", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, stringtools.IDCodeCastStringToSemverErr)))
	}

	return out, nil
}

// ValidateSemver return true if input version is valid.
func ValidateSemver(ctx context.Context, v string) bool {
	_, err := ParseSemverVersion(ctx, v)
	return err == nil
}

// IncrementSemverPatchVersion returns version with incremented patch version.
func IncrementSemverPatchVersion(ctx context.Context, v string) (string, error) {
	version, err := ParseSemverVersion(ctx, v)
	if err != nil {
		return "", err
	}
	if err := version.IncrementPatch(); err != nil {
		return "", wrappederror.New(err, "[IncrementPatchVersion] can not increment version", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, stringtools.IDCodeIncrementSemverErr)))
	}

	return version.String(), nil
}

// CompareSemver compares version left to right:
//
// -1 == left is less than right;
//
// 0 == left is equal to right;
//
// 1 == left is greater than right;
func CompareSemver(ctx context.Context, left, right string) (int, error) {
	leftVersion, err := ParseSemverVersion(ctx, left)
	if err != nil {
		return 0, err
	}
	rightVersion, err := ParseSemverVersion(ctx, right)
	if err != nil {
		return 0, err
	}

	return leftVersion.Compare(rightVersion), nil
}

// ReturnGreaterOrIncreasePatchVersion returns version.
//
// in case left is greater than right: returns left;
//
// in case left equals to right: returns left with increased patch;
//
// in case right is greater than left: returns right;
func ReturnGreaterOrIncreasePatchVersion(ctx context.Context, left, right string) (string, error) {
	result, err := CompareSemver(ctx, left, right)
	if err != nil {
		return "", err
	}

	switch result {
	case -1:
		return right, nil
	case 0:
		return IncrementSemverPatchVersion(ctx, left)
	case 1:
		return left, nil
	}

	return "", wrappederror.New(err, fmt.Sprintf("can not compare semver versions. Expects 1, 0, or -1 but received %d", result), wrappederror.NewCodeOption(code.MustNewInternalError(ctx, stringtools.IDCodeCompareSemverErr)))
}
