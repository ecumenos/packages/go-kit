package stringtools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// String Utils Codes
var (
	// IDCodeCastStringToSemverErr ???
	IDCodeCastStringToSemverErr = code.MustNewIDCode(code.StringUtilsPrefix, 0)
	// IDCodeIncrementSemverErr ???
	IDCodeIncrementSemverErr = code.MustNewIDCode(code.StringUtilsPrefix, 1)
	// IDCodeCompareSemverErr ???
	IDCodeCompareSemverErr = code.MustNewIDCode(code.StringUtilsPrefix, 2)
)
