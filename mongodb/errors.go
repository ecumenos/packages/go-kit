package mongodb

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// MongoDB Codes
var (
	// IDCodeMongoDBPingErr ???
	IDCodeMongoDBPingErr = code.MustNewIDCode(code.MongoDBPrefix, 0)
	// IDCodeMongoDBDisconnectErr ???
	IDCodeMongoDBDisconnectErr = code.MustNewIDCode(code.MongoDBPrefix, 1)
	// IDCodeMongoDBInsertErr ???
	IDCodeMongoDBInsertErr = code.MustNewIDCode(code.MongoDBPrefix, 2)
	// IDCodeMongoDBCountErr ???
	IDCodeMongoDBCountErr = code.MustNewIDCode(code.MongoDBPrefix, 3)
	// IDCodeMongoDBFindErr ???
	IDCodeMongoDBFindErr = code.MustNewIDCode(code.MongoDBPrefix, 4)
	// IDCodeMongoDBUpdateErr ???
	IDCodeMongoDBUpdateErr = code.MustNewIDCode(code.MongoDBPrefix, 5)
	// IDCodeMongoDBDeleteErr ???
	IDCodeMongoDBDeleteErr = code.MustNewIDCode(code.MongoDBPrefix, 6)
	// IDCodeMongoDBTruncateErr ???
	IDCodeMongoDBTruncateErr = code.MustNewIDCode(code.MongoDBPrefix, 7)
)
