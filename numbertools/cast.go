package numbertools

import (
	"context"
	"fmt"
	"strconv"

	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Float64ToBytes ???
func Float64ToBytes(in float64) []byte {
	return []byte(Float64ToString(in))
}

// BytesToFloat64 ???
func BytesToFloat64(ctx context.Context, in []byte) (float64, error) {
	return StringToFloat64(ctx, string(in))
}

// Float64ToString ???
func Float64ToString(in float64) string {
	return fmt.Sprint(in)
}

// StringToFloat64 ???
func StringToFloat64(ctx context.Context, in string) (float64, error) {
	out, err := strconv.ParseFloat(in, 64)
	if err != nil {
		return 0, wrappederror.New(err, "Can not convert", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeCastStrToFloat64Err)))
	}

	return out, nil
}

// StringToFloat32 ???
func StringToFloat32(ctx context.Context, in string) (float32, error) {
	out, err := strconv.ParseFloat(in, 32)
	if err != nil {
		return 0, wrappederror.New(err, "Can not convert", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeCastStrToFloat32Err)))
	}

	return float32(out), nil
}

// StringToInt ???
func StringToInt(ctx context.Context, in string) (int, error) {
	out, err := strconv.Atoi(in)
	if err != nil {
		return 0, wrappederror.New(err, "Can not convert", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, IDCodeCastStrToIntErr)))
	}

	return out, nil
}
