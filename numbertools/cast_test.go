package numbertools_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/numbertools"
)

func TestFloat64ToBytes(t *testing.T) {
	tCases := map[string]struct {
		in  float64
		out []byte
	}{
		"should convert 1": {
			in:  1,
			out: []byte(fmt.Sprint(1)),
		},
		"should convert 1.1": {
			in:  1.1,
			out: []byte(fmt.Sprint(1.1)),
		},
		"should convert 0.1": {
			in:  0.1,
			out: []byte(fmt.Sprint(0.1)),
		},
	}

	for name, tc := range tCases {
		t.Run(name, func(tt *testing.T) {
			out := numbertools.Float64ToBytes(tc.in)
			assert.Equal(tt, string(tc.out), string(out))
		})
	}
}

func TestBytesToFloat64(t *testing.T) {
	ctx := context.Background()
	tCases := map[string]struct {
		in  []byte
		out float64
	}{
		"should convert 1": {
			in:  numbertools.Float64ToBytes(1),
			out: 1,
		},
		"should convert 1.1": {
			in:  numbertools.Float64ToBytes(1.1),
			out: 1.1,
		},
		"should convert 0.1": {
			in:  numbertools.Float64ToBytes(0.1),
			out: 0.1,
		},
	}

	for name, tc := range tCases {
		t.Run(name, func(tt *testing.T) {
			out, err := numbertools.BytesToFloat64(ctx, tc.in)
			require.NoError(tt, err)
			assert.Equal(tt, tc.out, out)
		})
	}
}
