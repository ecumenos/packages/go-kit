package numbertools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Number Utils Codes
var (
	// IDCodeCastStrToFloat64Err ???
	IDCodeCastStrToFloat64Err = code.MustNewIDCode(code.NumberUtilsPrefix, 0)
	// IDCodeCastStrToIntErr ???
	IDCodeCastStrToIntErr = code.MustNewIDCode(code.NumberUtilsPrefix, 1)
	// IDCodeCastStrToFloat32Err ???
	IDCodeCastStrToFloat32Err = code.MustNewIDCode(code.NumberUtilsPrefix, 2)
)
