package numbertools

import (
	"math"
	"sort"
)

// Average ???
func Average(x []float64) float64 {
	if len(x) == 0 {
		return 0
	}

	var total float64
	for _, v := range x {
		total += v
	}
	return total / float64(len(x))
}

// Median ???
func Median(x []float64) float64 {
	if len(x) == 0 {
		return 0
	}

	sort.Float64s(x)

	half := len(x) / 2
	if isOdd(x) {
		return x[half]
	}

	return (x[half-1] + x[half]) / 2
}

// isOdd ???
func isOdd(x []float64) bool {
	return len(x)%2 != 0
}

// RoundFloat64 rounds value
func RoundFloat64(val float64, dec int) float64 {
	mul := math.Pow10(dec)

	return math.Round(val*mul) / mul
}
