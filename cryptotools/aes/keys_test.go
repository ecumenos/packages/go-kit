package aes_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/aes"
)

func TestGenerateKey(t *testing.T) {
	require := require.New(t)

	ctx := context.Background()
	key, err := aes.GenerateKey(ctx)
	require.NoError(err)
	t.Log(string(key))

	f, err := os.Create("aes_key")
	require.NoError(err)

	_, err = f.Write(key)
	require.NoError(err)
	require.NoError(f.Close())

}
