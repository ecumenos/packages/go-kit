package aes_test

import (
	"bytes"
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/aes"
)

func TestAES(t *testing.T) {
	require := require.New(t)

	key, err := os.ReadFile("aes_key")
	require.NoError(err)

	ctx := context.Background()
	msg := []byte("hello world")
	enc, err := aes.Encrypt(ctx, key, msg)
	require.NoError(err)

	dec, err := aes.Decrypt(ctx, key, enc)
	require.NoError(err)

	if !bytes.Equal(msg, dec) {
		t.Fail()
	}
}
