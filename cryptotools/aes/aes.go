package aes

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Encrypt encrypts message by key with AES cipher.
func Encrypt(ctx context.Context, key, msg []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, wrappederror.New(err, "can not encrypt", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeAESGenCipherErr)))
	}

	encrypted := make([]byte, aes.BlockSize+len(msg))
	iv := encrypted[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return nil, wrappederror.New(err, "can not encrypt", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeAESEncryptErr)))
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(encrypted[aes.BlockSize:], msg)

	return encrypted, nil
}

// Decrypt decrypts message by key with AES cipher.
func Decrypt(ctx context.Context, key, encrypted []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, wrappederror.New(err, "can not decrypt", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeAESGenCipherErr)))
	}

	if len(encrypted) < aes.BlockSize {
		return nil, wrappederror.New(err, "can not decrypt", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeAESInvalidBlockSizeErr)))
	}

	iv := encrypted[:aes.BlockSize]
	encrypted = encrypted[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(encrypted, encrypted)

	return encrypted, nil
}
