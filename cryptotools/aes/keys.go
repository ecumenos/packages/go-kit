package aes

import (
	"context"
	"crypto/rand"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// GenerateKey returns random 32 byte key for AES-256 cipher.
func GenerateKey(ctx context.Context) ([]byte, error) {
	b := make([]byte, 32) //generate a random 32 byte key for AES-256
	if _, err := rand.Read(b); err != nil {
		return nil, wrappederror.New(err, "can not generate key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeAESKeysGenErr)))
	}

	return b, nil
}
