package ecdsa_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/ecdsa"
)

func TestGenerateKeys(t *testing.T) {
	require := require.New(t)

	ctx := context.Background()
	// Create the keys
	keyPair, err := ecdsa.GenerateKeyPair(ctx)
	require.NoError(err)

	// Export the keys to pem string
	privPem, err := ecdsa.ExportECPrivateKey(ctx, keyPair.Private)
	require.NoError(err)
	f, err := os.Create("ecdsa_key")
	require.NoError(err)
	_, err = f.Write([]byte(privPem))
	require.NoError(err)
	require.NoError(f.Close())

	pubPem, err := ecdsa.ExportECPublicKey(ctx, keyPair.Public)
	require.NoError(err)
	f, err = os.Create("ecdsa_key.pub")
	require.NoError(err)
	_, err = f.Write([]byte(pubPem))
	require.NoError(err)
	require.NoError(f.Close())

	// Import the keys from pem string
	privParsed, err := ecdsa.ParseECPrivateKeyFromPEM(ctx, privPem)
	require.NoError(err)
	pubParsed, err := ecdsa.ParseECPublicKeyFromPEM(ctx, pubPem)
	require.NoError(err)

	// Export the newly imported keys
	privParsedPem, err := ecdsa.ExportECPrivateKey(ctx, privParsed)
	require.NoError(err)
	pubParsedPem, err := ecdsa.ExportECPublicKey(ctx, pubParsed)
	require.NoError(err)

	t.Log(string(privParsedPem))
	t.Log(string(pubParsedPem))

	// Check that the exported/imported keys match the original keys
	if string(privPem) != string(privParsedPem) || string(pubPem) != string(pubParsedPem) {
		t.Log("Failure: Export and Import did not result in same Keys")
		t.Fail()
	} else {
		t.Log("Success")
	}
}
