package ecdsa

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// KeyPair is RSA key pair.
type KeyPair struct {
	Private *ecdsa.PrivateKey
	Public  *ecdsa.PublicKey
}

// GenerateKeyPair returns private & public keys.
func GenerateKeyPair(ctx context.Context) (KeyPair, error) {
	private, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	if err != nil {
		return KeyPair{}, wrappederror.New(err, "can not generate key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAKeysGenErr)))
	}

	return KeyPair{
		Private: private,
		Public:  &private.PublicKey,
	}, nil
}

// ParseECPrivateKeyFromPEM Parse PEM encoded Elliptic Curve Private Key Structure.
func ParseECPrivateKeyFromPEM(ctx context.Context, key []byte) (*ecdsa.PrivateKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, wrappederror.New(err, "invalid key: Key must be PEM encoded PKCS1 or PKCS8 private key.", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAParsePrivKeyErr)))
	}

	// Parse the key
	var parsedKey interface{}
	if parsedKey, err = x509.ParseECPrivateKey(block.Bytes); err != nil {
		return nil, wrappederror.New(err, "parse private key failure", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAParsePrivKeyErr)))
	}

	var pkey *ecdsa.PrivateKey
	var ok bool
	if pkey, ok = parsedKey.(*ecdsa.PrivateKey); !ok {
		return nil, wrappederror.New(err, "key is not a valid ECDSA private key.", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAParsePrivKeyErr)))
	}

	return pkey, nil
}

// ExportECPrivateKey converts private key to []byte.
func ExportECPrivateKey(ctx context.Context, privateKey *ecdsa.PrivateKey) ([]byte, error) {
	x509Encoded, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		return nil, wrappederror.New(err, "can not marshal EC private key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAMarshalPrivKeyErr)))
	}
	pemEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})

	return pemEncoded, nil
}

// ParseECPublicKeyFromPEM Parse PEM encoded PKCS1 or PKCS8 public key.
func ParseECPublicKeyFromPEM(ctx context.Context, key []byte) (*ecdsa.PublicKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, wrappederror.New(err, "invalid key: Key must be PEM encoded PKCS1 or PKCS8 private key.", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAParsePubKeyErr)))
	}

	// Parse the key
	var parsedKey interface{}
	if parsedKey, err = x509.ParsePKIXPublicKey(block.Bytes); err != nil {
		if cert, err := x509.ParseCertificate(block.Bytes); err == nil {
			parsedKey = cert.PublicKey
		} else {
			return nil, wrappederror.New(err, "can not parse public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAParsePubKeyErr)))
		}
	}

	var pkey *ecdsa.PublicKey
	var ok bool
	if pkey, ok = parsedKey.(*ecdsa.PublicKey); !ok {
		return nil, wrappederror.New(err, "key is not a valid ECDSA public key.", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAParsePubKeyErr)))
	}

	return pkey, nil
}

// ExportECPublicKey converts public key to []byte.
func ExportECPublicKey(ctx context.Context, publicKey *ecdsa.PublicKey) ([]byte, error) {
	x509EncodedPub, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return nil, wrappederror.New(err, "can not marshal EC public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeECDSAMarshalPubKeyErr)))
	}
	pemEncodedPub := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

	return pemEncodedPub, nil
}
