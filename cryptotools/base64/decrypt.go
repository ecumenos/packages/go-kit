package base64

import (
	"context"
	"encoding/base64"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Decrypt decrypts input from base64 to string.
func Decrypt(ctx context.Context, in string) (string, error) {
	data, err := base64.StdEncoding.DecodeString(in)
	if err != nil {
		return "", wrappederror.New(err, "can not decrypt", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeBase64DecryptErr)))
	}
	return string(data), nil
}
