package base64_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/base64"
)

func TestEncrypt(t *testing.T) {
	ctx := context.Background()
	str := "string"
	enc := base64.Encrypt(str)
	dec, err := base64.Decrypt(ctx, enc)
	require.NoError(t, err)
	assert.Equal(t, dec, str)
}
