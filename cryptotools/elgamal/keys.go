package elgamal

import (
	"context"
	"crypto/rand"
	"io"
	"math/big"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"golang.org/x/crypto/openpgp/elgamal"
)

// Generate generate an elgamal key pair.
func Generate(ctx context.Context, priv *elgamal.PrivateKey, rand io.Reader) (err error) {
	priv.P = elgp
	priv.G = elgg
	xBytes := make([]byte, priv.P.BitLen()/8)
	_, err = io.ReadFull(rand, xBytes)
	if err != nil {
		return wrappederror.New(nil, "can not read x bytes for elgamal generation", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeElGamalKeysGenErr)))
	}
	// set private key
	priv.X = new(big.Int).SetBytes(xBytes)
	// compute public key
	priv.Y = new(big.Int).Exp(priv.G, priv.X, priv.P)

	return nil
}

// createElgamalPublicKey creates an elgamal public key from byte slice.
func createElgamalPublicKey(data []byte) *elgamal.PublicKey {
	if len(data) != 256 {
		return nil
	}

	return &elgamal.PublicKey{
		G: elgg,
		P: elgp,
		Y: new(big.Int).SetBytes(data),
	}
}

// createElgamalPrivateKey creates an elgamal private key from byte slice.
func createElgamalPrivateKey(data []byte) *elgamal.PrivateKey {
	if len(data) != 256 {
		return nil
	}

	x := new(big.Int).SetBytes(data)
	y := new(big.Int).Exp(elgg, x, elgp)

	return &elgamal.PrivateKey{
		PublicKey: elgamal.PublicKey{
			Y: y,
			G: elgg,
			P: elgp,
		},
		X: x,
	}
}

// createElgamalEncryption creates a new elgamal encryption session.
func createElgamalEncryption(ctx context.Context, pub *elgamal.PublicKey, rand io.Reader) (*EncrypterImpl, error) {
	kbytes := make([]byte, 256)
	k := new(big.Int)
	var err error
	for err == nil {
		_, err = io.ReadFull(rand, kbytes)
		k = new(big.Int).SetBytes(kbytes)
		k = k.Mod(k, pub.P)
		if k.Sign() != 0 {
			break
		}
	}
	if err != nil {
		return nil, wrappederror.New(err, "can not create elgamal bytes for enc", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeElGamalEncryptErr)))
	}

	return &EncrypterImpl{
		p:  pub.P,
		a:  new(big.Int).Exp(pub.G, k, pub.P),
		b1: new(big.Int).Exp(pub.Y, k, pub.P),
	}, nil
}

// PublicKey is elgamal public key.
type PublicKey [256]byte

// PrivateKey is elgamal private key.
type PrivateKey [256]byte

// Len returns elgamal public key length.
func (elg PublicKey) Len() int {
	return len(elg)
}

// NewEncrypter is encrypter constructor.
func (elg PublicKey) NewEncrypter(ctx context.Context) (Encrypter, error) {
	k := createElgamalPublicKey(elg[:])
	return createElgamalEncryption(ctx, k, rand.Reader)
}

// Len returns elgamal private key length.
func (elg PrivateKey) Len() int {
	return len(elg)
}

// NewDecrypter is decrypter constructor.
func (elg PrivateKey) NewDecrypter() (Decrypter, error) {
	return &decrypterImpl{
		k: createElgamalPrivateKey(elg[:]),
	}, nil
}
