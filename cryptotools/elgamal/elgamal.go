package elgamal

import (
	"context"
	"crypto/rand"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"golang.org/x/crypto/openpgp/elgamal"
)

// GenerateKeys returns elgamal private-public keys pair.
func GenerateKeys(ctx context.Context) (*elgamal.PrivateKey, *elgamal.PublicKey, error) {
	privKey := &elgamal.PrivateKey{}
	if err := Generate(ctx, privKey, rand.Reader); err != nil {
		return nil, nil, wrappederror.New(nil, "can not generate elgamal private-public keys pair", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeElGamalKeysGenErr)))
	}
	pubKey := createElgamalPublicKey(privKey.Y.Bytes())

	return privKey, pubKey, nil
}

// Encrypt encrypts byte slice with elgamal public key.
func Encrypt(ctx context.Context, pubKey *elgamal.PublicKey, input []byte) ([]byte, error) {
	for i := 0; i < 3; i++ {
		enc, err := encrypt(ctx, pubKey, input)
		if err == nil {
			return enc, nil
		}
	}

	return nil, wrappederror.New(nil, "can not encrypt input with elgamal public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeElGamalEncryptErr)))
}

func encrypt(ctx context.Context, pubKey *elgamal.PublicKey, data []byte) ([]byte, error) {
	encrypter, err := createElgamalEncryption(ctx, pubKey, rand.Reader)
	if err != nil {
		panic(err.Error())
	}
	encrypted, err := encrypter.Encrypt(ctx, data)
	if err != nil {
		return nil, wrappederror.New(err, "can not encrypt input with elgamal public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeElGamalEncryptErr)))
	}

	return encrypted, nil
}

// Decrypt decrypts byte slice with elgamal private key.
func Decrypt(ctx context.Context, privKey *elgamal.PrivateKey, d []byte) ([]byte, error) {
	decrypter := &decrypterImpl{
		k: privKey,
	}
	decrypted, err := decrypter.Decrypt(ctx, d)
	if err != nil {
		return nil, wrappederror.New(err, "can not decrypt input with elgamal private key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeElGamalDecryptErr)))
	}

	return decrypted, nil
}
