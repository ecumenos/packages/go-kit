package elgamal_test

import (
	"bytes"
	"context"
	"crypto/rand"
	"io"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/elgamal"
)

func TestElGamal(t *testing.T) {
	require := require.New(t)

	ctx := context.Background()

	tCases := []struct {
		name  string
		times int
	}{
		{
			name:  "should be ok for 100 times",
			times: 100,
		},
	}

	for _, tc := range tCases {
		t.Run(tc.name, func(t *testing.T) {
			for i := 0; i < tc.times; i++ {
				privKey, pubKey, err := elgamal.GenerateKeys(ctx)
				require.NoError(err)

				input := make([]byte, 222)
				_, _ = io.ReadFull(rand.Reader, input)
				encrypted, err := elgamal.Encrypt(ctx, pubKey, input)
				require.NoError(err)

				output, err := elgamal.Decrypt(ctx, privKey, encrypted)
				require.NoError(err)

				if !bytes.Equal(input, output) {
					t.Fail()
				}
			}
		})
	}
}
