package cryptotools

import "gitlab.com/ecumenos/packages/go-kit/errors/code"

// Codes
var (
	// IDCodeAESKeysGenErr ???
	IDCodeAESKeysGenErr = code.MustNewIDCode(code.CryptoPrefix, 0)
	// IDCodeAESGenCipherErr ???
	IDCodeAESGenCipherErr = code.MustNewIDCode(code.CryptoPrefix, 1)
	// IDCodeAESInvalidBlockSizeErr ???
	IDCodeAESInvalidBlockSizeErr = code.MustNewIDCode(code.CryptoPrefix, 2)
	// IDCodeAESEncryptErr ???
	IDCodeAESEncryptErr = code.MustNewIDCode(code.CryptoPrefix, 3)
	// IDCodeAESDecryptErr ???
	IDCodeAESDecryptErr = code.MustNewIDCode(code.CryptoPrefix, 4)
	// IDCodeAESKeysNotFound ???
	IDCodeAESKeysNotFound = code.MustNewIDCode(code.CryptoPrefix, 5)
	// IDCodeBase64EncryptErr ???
	IDCodeBase64EncryptErr = code.MustNewIDCode(code.CryptoPrefix, 10)
	// IDCodeBase64DecryptErr ???
	IDCodeBase64DecryptErr = code.MustNewIDCode(code.CryptoPrefix, 11)
	// IDCodeHashErr ???
	IDCodeHashErr = code.MustNewIDCode(code.CryptoPrefix, 12)
	// IDCodeDSAKeysGenErr ???
	IDCodeDSAKeysGenErr = code.MustNewIDCode(code.CryptoPrefix, 20)
	// IDCodeDSAKeyGenYieldedAllZerosErr ???
	IDCodeDSAKeyGenYieldedAllZerosErr = code.MustNewIDCode(code.CryptoPrefix, 21)
	// IDCodeDSAGenPubKeyFromPrivErr ???
	IDCodeDSAGenPubKeyFromPrivErr = code.MustNewIDCode(code.CryptoPrefix, 22)
	// IDCodeDSACreateSignerErr ???
	IDCodeDSACreateSignerErr = code.MustNewIDCode(code.CryptoPrefix, 23)
	// IDCodeDSASignErr ???
	IDCodeDSASignErr = code.MustNewIDCode(code.CryptoPrefix, 24)
	// IDCodeDSACreateVerifierErr ???
	IDCodeDSACreateVerifierErr = code.MustNewIDCode(code.CryptoPrefix, 25)
	// IDCodeDSAVerifyErr ???
	IDCodeDSAVerifyErr = code.MustNewIDCode(code.CryptoPrefix, 26)
	// IDCodeDSAInvalidPrivKeyFmtErr ???
	IDCodeDSAInvalidPrivKeyFmtErr = code.MustNewIDCode(code.CryptoPrefix, 27)
	// IDCodeDSAInvalidPubKeySigErr ???
	IDCodeDSAInvalidPubKeySigErr = code.MustNewIDCode(code.CryptoPrefix, 28)
	// IDCodeDSABadPubKeySigSizeErr ???
	IDCodeDSABadPubKeySigSizeErr = code.MustNewIDCode(code.CryptoPrefix, 29)
	// IDCodeDSAEncryptErr ???
	IDCodeDSAEncryptErr = code.MustNewIDCode(code.CryptoPrefix, 30)
	// IDCodeDSADecryptErr ???
	IDCodeDSADecryptErr = code.MustNewIDCode(code.CryptoPrefix, 31)
	// IDCodeDSAKeysNotFound ???
	IDCodeDSAKeysNotFound = code.MustNewIDCode(code.CryptoPrefix, 32)
	// IDCodeECDSAKeysGenErr ???
	IDCodeECDSAKeysGenErr = code.MustNewIDCode(code.CryptoPrefix, 40)
	// IDCodeECDSAEncryptErr ???
	IDCodeECDSAEncryptErr = code.MustNewIDCode(code.CryptoPrefix, 41)
	// IDCodeECDSADecryptErr ???
	IDCodeECDSADecryptErr = code.MustNewIDCode(code.CryptoPrefix, 42)
	// IDCodeECDSAKeysNotFound ???
	IDCodeECDSAKeysNotFound = code.MustNewIDCode(code.CryptoPrefix, 43)
	// IDCodeECDSAParsePrivKeyErr ???
	IDCodeECDSAParsePrivKeyErr = code.MustNewIDCode(code.CryptoPrefix, 44)
	// IDCodeECDSAParsePubKeyErr ???
	IDCodeECDSAParsePubKeyErr = code.MustNewIDCode(code.CryptoPrefix, 45)
	// IDCodeECDSAMarshalPrivKeyErr ???
	IDCodeECDSAMarshalPrivKeyErr = code.MustNewIDCode(code.CryptoPrefix, 46)
	// IDCodeECDSAMarshalPubKeyErr ???
	IDCodeECDSAMarshalPubKeyErr = code.MustNewIDCode(code.CryptoPrefix, 47)
	// IDCodeElGamalKeysGenErr ???
	IDCodeElGamalKeysGenErr = code.MustNewIDCode(code.CryptoPrefix, 60)
	// IDCodeElGamalEncryptErr ???
	IDCodeElGamalEncryptErr = code.MustNewIDCode(code.CryptoPrefix, 61)
	// IDCodeElGamalDecryptErr ???
	IDCodeElGamalDecryptErr = code.MustNewIDCode(code.CryptoPrefix, 62)
	// IDCodeElGamalKeysNotFound ???
	IDCodeElGamalKeysNotFound = code.MustNewIDCode(code.CryptoPrefix, 63)
	// IDCodeRSAKeysGenErr ???
	IDCodeRSAKeysGenErr = code.MustNewIDCode(code.CryptoPrefix, 70)
	// IDCodeRSAEncryptErr ???
	IDCodeRSAEncryptErr = code.MustNewIDCode(code.CryptoPrefix, 71)
	// IDCodeRSADecryptErr ???
	IDCodeRSADecryptErr = code.MustNewIDCode(code.CryptoPrefix, 72)
	// IDCodeRSAKeysNotFound ???
	IDCodeRSAKeysNotFound = code.MustNewIDCode(code.CryptoPrefix, 73)
	// IDCodeRSAParsePEMBlockErr ???
	IDCodeRSAParsePEMBlockErr = code.MustNewIDCode(code.CryptoPrefix, 74)
	// IDCodeRSAParsePKCS1PrivKeyErr ???
	IDCodeRSAParsePKCS1PrivKeyErr = code.MustNewIDCode(code.CryptoPrefix, 75)
	// IDCodeRSAMarshalPKIXPubKeyErr ???
	IDCodeRSAMarshalPKIXPubKeyErr = code.MustNewIDCode(code.CryptoPrefix, 76)
	// IDCodeRSAParsePKIXPubKeyErr ???
	IDCodeRSAParsePKIXPubKeyErr = code.MustNewIDCode(code.CryptoPrefix, 77)
	// IDCodeNoRSAKeyTypeErr ???
	IDCodeNoRSAKeyTypeErr = code.MustNewIDCode(code.CryptoPrefix, 78)
)
