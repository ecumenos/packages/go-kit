package rsa

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Encrypt encrypts input with public key.
func Encrypt(ctx context.Context, pk *rsa.PublicKey, in, label []byte) ([]byte, error) {
	out, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, pk, in, label)
	if err != nil {
		return nil, wrappederror.New(err, "can not encrypt with RSA", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAEncryptErr)))
	}

	return out, nil
}
