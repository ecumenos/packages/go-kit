package rsa

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// KeyPair is RSA key pair.
type KeyPair struct {
	Private *rsa.PrivateKey
	Public  *rsa.PublicKey
}

// GenerateKeyPair returns private & public keys.
func GenerateKeyPair(ctx context.Context) (KeyPair, error) {
	// The GenerateKey method takes in a reader that returns random bits, and
	// the number of bits
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return KeyPair{}, wrappederror.New(err, "can not generate RSA key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAKeysGenErr)))
	}

	// The public key is a part of the *rsa.PrivateKey struct

	return KeyPair{
		Private: privateKey,
		Public:  &privateKey.PublicKey,
	}, nil
}

// ExportRSAPrivateKeyAsPEMString converts private key to string.
func ExportRSAPrivateKeyAsPEMString(privateKey *rsa.PrivateKey) string {
	privkeyBytes := x509.MarshalPKCS1PrivateKey(privateKey)
	privkeyPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: privkeyBytes,
		},
	)

	return string(privkeyPEM)
}

// ParseRSAPrivateKeyFromPEMString converts string private key to private key.
func ParseRSAPrivateKeyFromPEMString(ctx context.Context, privatePEM string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(privatePEM))
	if block == nil {
		return nil, wrappederror.New(nil, "failed to parse PEM block containing the key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAParsePEMBlockErr)))
	}

	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, wrappederror.New(err, "can not parse PKCS1 private key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAParsePKCS1PrivKeyErr)))
	}

	return priv, nil
}

// ExportRSAPublicKeyAsPEMString converts public key to string.
func ExportRSAPublicKeyAsPEMString(ctx context.Context, pubkey *rsa.PublicKey) (string, error) {
	pubkeyBytes, err := x509.MarshalPKIXPublicKey(pubkey)
	if err != nil {
		return "", wrappederror.New(err, "can not marshal PKIX public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAMarshalPKIXPubKeyErr)))
	}
	pubkeyPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: pubkeyBytes,
		},
	)

	return string(pubkeyPEM), nil
}

// ParseRSAPublicKeyFromPEMString converts string public key to public key.
func ParseRSAPublicKeyFromPEMString(ctx context.Context, pubPEM string) (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(pubPEM))
	if block == nil {
		return nil, wrappederror.New(nil, "failed to parse PEM block containing the key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAParsePEMBlockErr)))
	}

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, wrappederror.New(err, "can not parse PKIX public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSAParsePKIXPubKeyErr)))
	}

	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return pub, nil
	default:
		break // fall through
	}

	return nil, wrappederror.New(nil, "key type is not RSA", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeNoRSAKeyTypeErr)))
}
