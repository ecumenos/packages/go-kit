package rsa_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/rsa"
)

func TestEncrypt(t *testing.T) {
	require := require.New(t)

	ctx := context.Background()

	privPEM, err := os.ReadFile("id_rsa")
	require.NoError(err)

	pubPEM, err := os.ReadFile("id_rsa.pub")
	require.NoError(err)

	privParsed, err := rsa.ParseRSAPrivateKeyFromPEMString(ctx, string(privPEM))
	require.NoError(err)
	pubParsed, err := rsa.ParseRSAPublicKeyFromPEMString(ctx, string(pubPEM))
	require.NoError(err)

	msg := "message"
	encrypted, err := rsa.Encrypt(ctx, pubParsed, []byte(msg), nil)
	require.NoError(err)

	decrypted, err := rsa.Decrypt(ctx, privParsed, encrypted)
	require.NoError(err)

	assert.Equal(t, msg, string(decrypted))
}
