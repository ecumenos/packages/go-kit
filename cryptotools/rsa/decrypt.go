package rsa

import (
	"context"
	"crypto"
	"crypto/rsa"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Decrypt decrypts input with private key.
func Decrypt(ctx context.Context, sk *rsa.PrivateKey, in []byte) ([]byte, error) {
	out, err := sk.Decrypt(nil, in, &rsa.OAEPOptions{Hash: crypto.SHA256})
	if err != nil {
		return nil, wrappederror.New(err, "can not decrypt input with RSA", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeRSADecryptErr)))
	}

	return out, nil
}
