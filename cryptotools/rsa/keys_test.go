package rsa_test

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/rsa"
)

func TestGenerateKeys(t *testing.T) {
	require := require.New(t)

	ctx := context.Background()

	// Create the keys
	keyPair, err := rsa.GenerateKeyPair(ctx)
	require.NoError(err)

	// Export the keys to pem string
	privPem := rsa.ExportRSAPrivateKeyAsPEMString(keyPair.Private)
	f, err := os.Create("id_rsa")
	require.NoError(err)
	_, err = f.Write([]byte(privPem))
	require.NoError(err)
	require.NoError(f.Close())

	pubPem, err := rsa.ExportRSAPublicKeyAsPEMString(ctx, keyPair.Public)
	require.NoError(err)
	f, err = os.Create("id_rsa.pub")
	require.NoError(err)
	_, err = f.Write([]byte(pubPem))
	require.NoError(err)
	require.NoError(f.Close())

	// Import the keys from pem string
	privParsed, err := rsa.ParseRSAPrivateKeyFromPEMString(ctx, privPem)
	require.NoError(err)
	pubParsed, err := rsa.ParseRSAPublicKeyFromPEMString(ctx, pubPem)
	require.NoError(err)

	// Export the newly imported keys
	privParsedPem := rsa.ExportRSAPrivateKeyAsPEMString(privParsed)
	pubParsedPem, err := rsa.ExportRSAPublicKeyAsPEMString(ctx, pubParsed)
	require.NoError(err)

	t.Log(privParsedPem)
	t.Log(pubParsedPem)

	// Check that the exported/imported keys match the original keys
	if privPem != privParsedPem || pubPem != pubParsedPem {
		t.Log("Failure: Export and Import did not result in same Keys")
		t.Fail()
	} else {
		t.Log("Success")
	}
}
