package dsa

import (
	"context"
	"crypto/dsa"
	"crypto/rand"
	"io"
	"math/big"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// generate generates a dsa keypair.
func generate(ctx context.Context, priv *dsa.PrivateKey, rand io.Reader) error {
	// put our paramters in
	priv.P = param.P
	priv.Q = param.Q
	priv.G = param.G
	// generate the keypair
	if err := dsa.GenerateKey(priv, rand); err != nil {
		return wrappederror.New(nil, "can not generate keys", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAKeysGenErr)))
	}

	return nil
}

// createPublicKey creates i2p dsa public key given its public component.
func createPublicKey(Y *big.Int) *dsa.PublicKey {
	return &dsa.PublicKey{
		Parameters: param,
		Y:          Y,
	}
}

// createPrivateKey creates i2p dsa private key given its public component.
func createPrivateKey(X *big.Int) *dsa.PrivateKey {
	if X.Cmp(dsap) != -1 {
		return nil
	}

	Y := new(big.Int)
	Y.Exp(dsag, X, dsap)

	return &dsa.PrivateKey{
		PublicKey: dsa.PublicKey{
			Parameters: param,
			Y:          Y,
		},
		X: X,
	}
}

// PrivateKey is DSA private key that has 20 bytes.
type PrivateKey [20]byte

// NewSigner create a new dsa signer.
func (k PrivateKey) NewSigner(ctx context.Context) (Signer, error) {
	return &SignerImpl{
		k: createPrivateKey(new(big.Int).SetBytes(k[:])),
	}, nil
}

// Public returns public key from DSA private key.
func (k PrivateKey) Public(ctx context.Context) (PublicKey, error) {
	p := createPrivateKey(new(big.Int).SetBytes(k[:]))
	if p == nil {
		return PublicKey{}, wrappederror.New(nil, "can not get public key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAInvalidPrivKeyFmtErr)))
	}

	var pk PublicKey
	copy(pk[:], p.Y.Bytes())

	return pk, nil
}

// Generate returns DSA private key  from parent DSA private key.
func (k PrivateKey) Generate(ctx context.Context) (s PrivateKey, err error) {
	dk := new(dsa.PrivateKey)
	err = generate(ctx, dk, rand.Reader)
	if err == nil {
		copy(k[:], dk.X.Bytes())
		s = k
	}
	if err != nil {
		err = wrappederror.New(err, "can not generate key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAKeysGenErr)))
	}

	return
}
