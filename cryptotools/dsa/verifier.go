package dsa

import (
	"context"
	"crypto/dsa"
	"crypto/sha1"
	"math/big"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// Verifier is type for verifying signatures.
type Verifier interface {
	// verify hashed data with this signing key
	// return nil on valid signature otherwise error
	VerifyHash(ctx context.Context, h, sig []byte) error
	// verify an unhashed piece of data by hashing it and calling VerifyHash
	Verify(ctx context.Context, data, sig []byte) error
}

// VerifierImpl is DSA verifier.
type VerifierImpl struct {
	k *dsa.PublicKey
}

// PublicKey is DSA public key that has 128 bytes.
type PublicKey [128]byte

// NewVerifier create a new DSA verifier.
func (k PublicKey) NewVerifier() (Verifier, error) {
	return &VerifierImpl{
		k: createPublicKey(new(big.Int).SetBytes(k[:])),
	}, nil
}

// Verify verifies data with a DSA public key.
func (v *VerifierImpl) Verify(ctx context.Context, data, sig []byte) error {
	h := sha1.Sum(data)

	return v.VerifyHash(ctx, h[:], sig)
}

// VerifyHash verifies hash of data with a DSA public key.
func (v *VerifierImpl) VerifyHash(ctx context.Context, h, sig []byte) error {
	if len(sig) != 40 {
		return wrappederror.New(nil, "can not verify data", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSABadPubKeySigSizeErr)))
	}

	r := new(big.Int).SetBytes(sig[:20])
	s := new(big.Int).SetBytes(sig[20:])
	if dsa.Verify(v.k, h, r, s) {
		return nil
	}

	return wrappederror.New(nil, "can not verify data", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAInvalidPubKeySigErr)))
}

// Len returns length of DSA public key.
func (k PublicKey) Len() int {
	return len(k)
}
