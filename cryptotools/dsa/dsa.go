package dsa

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
)

// GenerateKeys returns private-public DSA key.
func GenerateKeys(ctx context.Context) (PrivateKey, PublicKey, error) {
	var sk PrivateKey
	sk, err := sk.Generate(ctx)
	if err != nil {
		return PrivateKey{}, PublicKey{}, wrappederror.New(err, "can not generate key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAKeysGenErr)))
	}

	zeros := 0
	for _, b := range sk {
		if b == 0 {
			zeros++
		}
	}
	if zeros == len(sk) {
		return PrivateKey{}, PublicKey{}, wrappederror.New(err, "can not generate key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAKeyGenYieldedAllZerosErr)))
	}

	pk, err := sk.Public(ctx)
	if err != nil {
		return PrivateKey{}, PublicKey{}, wrappederror.New(err, "can not generate key", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAGenPubKeyFromPrivErr)))
	}

	return sk, pk, nil
}

// Sign signs input byte slice by DSA private key.
func Sign(ctx context.Context, sk PrivateKey, input []byte) ([]byte, error) {
	signer, err := sk.NewSigner(ctx)
	if err != nil {
		return nil, wrappederror.New(err, "can not sign data", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSACreateSignerErr)))
	}

	out, err := signer.Sign(ctx, input)
	if err != nil {
		return nil, wrappederror.New(err, "can not sign data", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSASignErr)))
	}

	return out, nil
}

// Verify verifies signed byte slice with input byte slice and DSA public key.
func Verify(ctx context.Context, pk PublicKey, input, signed []byte) error {
	verify, err := pk.NewVerifier()
	if err != nil {
		return wrappederror.New(err, "can not verify data", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSACreateVerifierErr)))
	}

	if err := verify.Verify(ctx, input, signed); err != nil {
		return wrappederror.New(err, "can not sign data", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeDSAVerifyErr)))
	}

	return nil
}
