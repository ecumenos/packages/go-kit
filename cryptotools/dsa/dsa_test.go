package dsa_test

import (
	"context"
	"crypto/rand"
	"io"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ecumenos/packages/go-kit/cryptotools/dsa"
)

func TestDSA(t *testing.T) {
	require := require.New(t)

	ctx := context.Background()
	sk, pk, err := dsa.GenerateKeys(ctx)
	require.NoError(err)

	data := make([]byte, 512)
	_, err = io.ReadFull(rand.Reader, data)
	require.NoError(err)

	signed, err := dsa.Sign(ctx, sk, data)
	require.NoError(err)

	require.NoError(dsa.Verify(ctx, pk, data, signed))
}
