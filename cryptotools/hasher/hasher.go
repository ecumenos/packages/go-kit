package hasher

import (
	"context"

	"gitlab.com/ecumenos/packages/go-kit/cryptotools"
	"gitlab.com/ecumenos/packages/go-kit/errors/code"
	"gitlab.com/ecumenos/packages/go-kit/errors/wrappederror"
	"golang.org/x/crypto/bcrypt"
)

// Hash hashes input string with static salt & dynamic salt.
func Hash(ctx context.Context, in, ss, ds string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(ss+in+ds), 14)
	if err != nil {
		return "", wrappederror.New(err, "can not hash string", wrappederror.NewCodeOption(code.MustNewInternalError(ctx, cryptotools.IDCodeHashErr)))
	}

	return string(bytes), nil
}

// ValidateHash validates input with static slat & dynamic salt. It returns true if it is ok.
func ValidateHash(in, ss, ds, hashed string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(ss+in+ds))
	return err == nil
}
